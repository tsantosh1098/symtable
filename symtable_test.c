#include <stdio.h>
//#include <mcheck.h>
#include "symtable_link.c"


/* Helper function to print the binding - key, value pair */
void print_bindings(const char *pcKey, const void *pvValue, void *pvExtra)
{
    if(!strcmp((char *)pvExtra, "int") || !strcmp((char *)pvExtra, "integer"))
        printf("%s %d\n", pcKey, (int *)pvValue);
    else
        printf("%s %s\n", pcKey, (char *)pvValue);
}

void test_symtable_new()
{
    SymTable_t new = SymTable_new();
    if(new == NULL)
    {
        printf("ERROR : Memory Allocation failed\n");
        return;
    }
    SymTable_free(new);
    printf("PASS: SYMTABLE_NEW FUNCTION\n");
}

void test_symtable_getLength()
{
    SymTable_t NEW = SymTable_new();
    assert(SymTable_getLength(NEW) == 0);

    SymTable_put(NEW, "key1", "1");
    assert(SymTable_getLength(NEW) == 1);
    
    SymTable_put(NEW, "key2", "2");
    assert(SymTable_getLength(NEW) == 2);

    SymTable_put(NEW, "key3", "3");
    assert(SymTable_getLength(NEW) == 3);

    SymTable_put(NEW, "key4", "4");
    assert(SymTable_getLength(NEW) == 4);

    SymTable_put(NEW, "key5", "5");
    assert(SymTable_getLength(NEW) == 5);

    SymTable_put(NEW, "key6", "6");
    assert(SymTable_getLength(NEW) == 6);

    SymTable_put(NEW, "key3", "4");
    assert(SymTable_getLength(NEW) == 6);

    SymTable_free(NEW);
    NEW = NULL;
    assert(SymTable_getLength(NEW) == -1);

    printf("PASS: SYMTABLE_GET FUNCTION\n");
}

void test_symtable_free()
{
    SymTable_t new;
    new = SymTable_new();
    SymTable_free(new);
    new = NULL;
    if (new != NULL)
    {
        printf("ERROR : Memory Deallocation failed\n");
        return;
    }
    printf("PASS: SYMTABLE_FREE FUNCTION\n");
}

void test_symtable_map()
{
    SymTable_t new;
    new = SymTable_new();
    SymTable_put(new, "key1", "1");
    SymTable_put(new, "key2", "2");
    SymTable_put(new, "key3", "3");
    SymTable_put(new, "key4", "4");
    printf("---------------------\nSYMTABLE 1\n");
    SymTable_map(new, &(print_bindings),"Integer");
    printf("---------------------\n");
    SymTable_free(new);
    new = NULL;
    printf("---------------------\n AFTER FREE\n");
    SymTable_map(new, &(print_bindings),"Integer");
    SymTable_put(new, "key4", "value4");
    new = SymTable_new();
    SymTable_put(new, "key1", "value1");
    SymTable_put(new, "key2", "value2");
    SymTable_put(new, "key3", "value3");
    SymTable_put(new, "key4", "value4");
    printf("---------------------\nSYMTABLE 2\n");
    SymTable_map(new, &(print_bindings),"");
    printf("---------------------\n");
    printf("PASS: SYMTABLE_MAP FUNCTION\n");
}

void test_SymTable_get()
{
    SymTable_t NEW = SymTable_new();
    
    SymTable_put(NEW, "key1", "value1");
    SymTable_put(NEW, "key2", "value2");
    SymTable_put(NEW, "key3", "value3");
    SymTable_put(NEW, "key4", "value4");
    SymTable_put(NEW, "", "random_value");

    assert(!strcmp(SymTable_get(NEW, "key4"), "value4"));

    assert(SymTable_get(NEW, "key5") == NULL);
    assert(SymTable_get(NEW, NULL) == NULL);
    assert(SymTable_get(NEW, "") == NULL);
    SymTable_free(NEW);
    NEW = NULL;
    
    assert(SymTable_get(NEW, "key1") == NULL);
    printf("PASS: SYMTABLE_GET FUNCTION\n");
}

/* Unit test for SymTable_put() function */
void test_SymTable_put()
{
    SymTable_t NEW = SymTable_new();
    assert(SymTable_put(NEW, "key1", "1") == 1);
    assert(SymTable_put(NEW, "key2", "2") == 1);
    assert(SymTable_put(NEW, "key3", "3") == 1);
    assert(SymTable_put(NEW, "key4", "") == 1);
    assert(SymTable_put(NEW, "key1", "-1") == 0);
    assert(SymTable_put(NEW, "key1", "0") == 0);
    assert(SymTable_put(NEW, "", "2") == 0);
    assert(SymTable_put(NEW, NULL, "1") == 0);
    SymTable_free(NEW);
    NEW = NULL;
    assert(SymTable_put(NEW, "key6", "100") == 0);
    printf("PASS:  SYMTABLE_PUT FUNCTION\n");
}

void test_SymTable_contains()
{
    SymTable_t NEW = SymTable_new();

    SymTable_put(NEW, "key1", "value");
    assert(SymTable_contains(NEW, "key1") == 1);
    assert(SymTable_contains(NEW, "key2") == 0);
    
    SymTable_put(NEW, "", "random_value");
    SymTable_put(NEW, NULL, "random_value");
    assert(SymTable_contains(NEW, "") == 0);
    assert(SymTable_contains(NEW, NULL) == 0);
    
    SymTable_free(NEW);
    NEW = NULL;
    assert(SymTable_contains(NEW, "key1") == 0);
    printf("PASS:  SYMTABLE_CONTAINS FUNCTION\n");
}

void test_SymTable_replace()
{
    SymTable_t NEW = SymTable_new();
    SymTable_put(NEW, "key1", "1");
    SymTable_put(NEW, "key2", "2");
    SymTable_put(NEW, "key3", "3");
    SymTable_put(NEW, "key4", "4");
    SymTable_put(NEW, "", "5");
    SymTable_put(NEW, NULL, "6");
    assert(!strcmp(SymTable_replace(NEW, "key1", "11"), "1"));
    assert(!strcmp(SymTable_replace(NEW, "key2", "12"), "2"));
    assert(!strcmp(SymTable_replace(NEW, "key3", "13"), "3"));
    assert(!strcmp(SymTable_replace(NEW, "key4", "14"), "4"));
    assert(!strcmp(SymTable_replace(NEW, "key4", "24"), "14"));
    assert(SymTable_replace(NEW, "key5", "15") == NULL);
    SymTable_free(NEW);
    NEW = NULL;
    assert(SymTable_replace(NEW, "key1", "1") == NULL);
    printf("PASS:  SYMTABLE_REPLACE FUNCTION\n");
}

/* Unit test for SymTable_remove() function */
void test_SymTable_remove()
{
    SymTable_t NEW = SymTable_new();
    SymTable_put(NEW, "key1", "1");
    SymTable_put(NEW, "key2", "2");
    SymTable_put(NEW, "key3", "3");
    SymTable_put(NEW, "key4", "4");
    SymTable_put(NEW, "", "5");
    SymTable_put(NEW, NULL, "5");
    assert(!strcmp(SymTable_remove(NEW, "key4"), "4"));
    assert(!strcmp(SymTable_remove(NEW, "key1"), "1"));
    assert(SymTable_remove(NEW, "key4") == NULL);
    assert(SymTable_remove(NEW, "") == NULL);
    assert(SymTable_remove(NEW, NULL) == NULL);
    SymTable_free(NEW);
    NEW = NULL;
    assert(SymTable_remove(NEW, "key1") == NULL);
    printf("PASS:  SYMTABLE_REMOVE FUNCTION\n");
}


void main()
{
    test_symtable_new();

    test_symtable_getLength();

    test_symtable_free();
    
    test_symtable_map();

    test_SymTable_get();

    test_SymTable_put();

    test_SymTable_contains();

    test_SymTable_replace();

    test_SymTable_remove();
    printf("ALL TEST CASED HAS BEEN PASSED\n");
}