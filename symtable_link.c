
#include "symtable.h"


struct BIND
{
    char * pcKey;
    void * pcValue;
    Bind *next;
};

struct SymTable
{
    int length;
    Bind *head;
};

/*
    To free a particular bind
*/
void Bind_free( Bind *bind)
{
    assert(bind != NULL);
    free(bind);
}

/*
    Creates  a new Table pointer by initializing the length = 0
    and pointer to binding as NULL
*/
SymTable_t SymTable_new(void)
{
    SymTable_t new_node;
    new_node = (SymTable_t)calloc(sizeof(struct SymTable),1);
    if (new_node != NULL)
    {
        new_node->head = NULL;
        new_node->length = 0;
        return new_node;
    }
    return NULL;
}

/*
    Frees all the bindings present in the Symtable 
    later the pointer gets freed.
*/
void SymTable_free(SymTable_t oSymTable)
{
    assert(oSymTable != NULL);
    Bind *nextBind,*currentBind;
    currentBind = oSymTable->head;
    // printf("FREE 1\t");
    while(currentBind!=NULL)
    {
        // printf("FREE 2\t");

        nextBind = currentBind->next;
        free(currentBind);
        currentBind = nextBind;
    }
    free(oSymTable);
    oSymTable = NULL;
}

/*
    Returns the number of bindings in the symTable
    If the symTable is null then it returns 0
*/
int SymTable_getLength (SymTable_t oSymTable)
{
    if(oSymTable==NULL){
        return -1;
    }
    return oSymTable->length;
}

void SymTable_map( SymTable_t oSymTable, void ( *pfApply ) (const char *pcKey, const void *pvValue, void *pvExtra),const void *pvExtra )
{
    if (oSymTable == NULL)
        return;

    Bind *current;
    current = oSymTable->head;
    while(current != NULL)
    {
        pfApply((const char*)current->pcKey, (const void *)current->pcValue, (void *)pvExtra);
        current = current->next;
    }
}

int SymTable_contains( SymTable_t oSymTable, const char *pcKey)
{
    if(oSymTable == NULL || pcKey == NULL || strcmp(pcKey,"")==0)
        return 0;
    Bind *currentBinding = oSymTable->head;
    while(currentBinding !=NULL)
    {
        if(strcmp(currentBinding->pcKey,pcKey)==0)
            return 1;   
        currentBinding = currentBinding->next;
    }
    return 0;
}

int SymTable_put( SymTable_t oSymTable, const char *pcKey, const void *pcValue)
{
    if(oSymTable == NULL || pcKey == NULL || strcmp(pcKey,"")==0){
        return 0;}

    if (SymTable_contains(oSymTable , pcKey) != 0)
        return 0;
    Bind *newBinding,*currentBinding;
    newBinding = (Bind*)malloc(sizeof(Bind)*1);
    if(newBinding == NULL)
        return 0;

    if(oSymTable->head == NULL)
    {
        newBinding->pcKey = (char *)pcKey;
        newBinding->pcValue = (void *)pcValue;
        newBinding->next = NULL;
        oSymTable->head = newBinding;
        oSymTable->length ++;
        return 1;
    }

    currentBinding = oSymTable->head;
    while(currentBinding->next != NULL)
    {
        currentBinding = currentBinding->next;
    }
    newBinding->pcKey = (char *)pcKey;
    newBinding->pcValue = (void *)pcValue;
    newBinding->next = NULL;
    currentBinding->next= newBinding;
    oSymTable->length ++;
    
    return 1;   
}

void* SymTable_get(SymTable_t oSymTable, const char *pcKey )
{
    // assert(oSymTable!=NULL);
    Bind *currentBinding;
    if (pcKey == NULL || strlen(pcKey)==0 || oSymTable == NULL)
        return NULL;
    currentBinding = oSymTable->head;
    while (currentBinding != NULL)
    {
        if(strcmp (currentBinding->pcKey , pcKey) == 0)
        {
            return currentBinding->pcValue;
        }
        currentBinding = currentBinding->next;
    }
    return NULL;
}


void* SymTable_replace( SymTable_t oSymTable, const char *pcKey, const void *pcValue )
{
    if(oSymTable == NULL)
        return NULL;
    Bind *temp = oSymTable->head;
    void* pcVa;
    while (temp!= NULL)
    {
        if(strcmp (temp->pcKey , pcKey) == 0)
        {
            pcVa = temp->pcValue;
            temp->pcValue = (void *)pcValue;
            return pcVa;
        }
        temp = temp->next;
    }
    return NULL;
}


void* SymTable_remove (SymTable_t oSymTable,const char *pcKey)
{
    if(oSymTable == NULL || pcKey == NULL || strcmp(pcKey,"")==0)
        return NULL;
    Bind *previous = oSymTable->head;
    Bind *current = oSymTable->head;
    void* pcVal;
    if(current->next == NULL)
    {
        if(strcmp (current->pcKey , pcKey) == 0)
        {
            oSymTable->head=NULL;
            pcVal = current->pcValue;
            Bind_free(current);
            oSymTable->length--;
            return pcVal;
        }
    }
    while (current->next != NULL)
    {
        if(strcmp (current->pcKey , pcKey) == 0)
        {
            if ( current == oSymTable->head)
            {
                oSymTable->head = current->next;
                pcVal = current->pcValue;
                Bind_free(current);
                oSymTable->length--;
                return pcVal;
            }
            previous->next = current->next;
            pcVal = current->pcValue;
            Bind_free(current);
            oSymTable->length--;
            return pcVal;
        }
        previous = current;
        current = current->next;
    }
    if(strcmp (current->pcKey , pcKey) == 0)
    {
        previous->next = current->next;
        pcVal = current->pcValue;
        Bind_free(current);
        oSymTable->length--;
        return pcVal;
    }
    
    return NULL;
}
