#include "symtable.h"
#include "hash.h"

struct BIND {
    char *pcKey;
    void *pcValue;
    Bind *next; 
};

struct SymTable
{
    int length;
    Bind *head;
};

//int bucketCounts[] = {509, 1021, 2039, 4093, 8191,16381, 32749,65521};
int bucketCounts[] = {3, 6, 9, 4093, 8191,16381, 32749,65521};
static int bucketCountid;

/*
    To free a particular bind
*/
void Bind_free( Bind *bind)
{
    assert(bind != NULL);
    free(bind);
}

/*
    Creates  a new Table pointer by initializing the length = 0
    and pointer to binding as NULL
*/
SymTable_t SymTable_new(void)
{
    SymTable_t new_node;
    new_node = (SymTable_t)calloc(sizeof(struct SymTable),bucketCounts[bucketCountid]);
    if (new_node != NULL)
    {
        for(int i=0;i<bucketCountid[bucketCounts];i++)
        {
            new_node[i].length = 0;
            new_node[i].head = NULL; 
        }
        return new_node;
    }
    return NULL;
}

/*
    Frees all the bindings present in the Symtable 
    later the pointer gets freed.
*/
void SymTable_free(SymTable_t oSymTable)
{
    assert(oSymTable != NULL);
    Bind *nextBind,*currentBind;
    for(int i=0;i<bucketCounts[bucketCountid];i++)
    {
        currentBind = oSymTable[i].head;
        while(currentBind!=NULL)
        {
            nextBind = currentBind->next;
            free(currentBind);
            currentBind = nextBind;
        }
    }
    free(oSymTable);
    oSymTable = NULL;
}

/*
    Returns the number of bindings in the symTable
    If the symTable is null then it returns 0
*/

int SymTable_getLength (SymTable_t oSymTable)
{
    if(oSymTable==NULL)
        return -1;
    int sum=0;
    for(int itr = 0; itr < bucketCounts[bucketCountid]; itr++)
    {
        sum+=oSymTable[itr].length;
    }
    return sum;
}

void SymTable_map( SymTable_t oSymTable, void ( *pfApply ) (const char *pcKey, const void *pvValue, void *pvExtra),const void *pvExtra )
{
    if (oSymTable == NULL)
        return;

    for(int itr = 0; itr < bucketCounts[bucketCountid]; itr++)
    {
        Bind *current;
        current = oSymTable[itr].head;
        while(current != NULL)
        {
            pfApply((const char*)current->pcKey, (const void *)current->pcValue, (void *)pvExtra);
            current = current->next;
        }
    }
}

int SymTable_contains( SymTable_t oSymTable, const char *pcKey)
{
    if(oSymTable == NULL || pcKey == NULL || strcmp(pcKey,"")==0)
        return 0;
    int idx = SymTable_hash(pcKey,bucketCounts[bucketCountid]);
    Bind *currentBinding = oSymTable[idx].head;
    while(currentBinding !=NULL)
    {
        if(strcmp(currentBinding->pcKey,pcKey)==0)
            return 1;   
        currentBinding = currentBinding->next;
    }
    
    return 0;
}

int SymTable_put( SymTable_t oSymTable, const char *pcKey, const void *pcValue)
{
    if(oSymTable == NULL || pcKey == NULL || strcmp(pcKey,"")==0){
        return 0;}

    // if oSymTable has more elements then bucketCount, we do rehash.
    printf("%d\n",SymTable_getLength(oSymTable));
    if(SymTable_getLength(oSymTable) >= bucketCounts[bucketCountid])
    {
        
        oSymTable = SymTable_rehash(oSymTable);
    }
      
    printf("%d\n",oSymTable);
    int idx = SymTable_hash(pcKey,bucketCounts[bucketCountid]);
    Bind *newBinding;
    newBinding = (Bind*)malloc(sizeof(Bind)*1);
    if(newBinding == NULL)
        return 0;
    newBinding->pcKey = pcKey;
    newBinding->pcValue = pcValue;
    newBinding->next = NULL;
    if(oSymTable[idx].length == 0)
    {
        oSymTable[idx].head = newBinding;
        oSymTable[idx].length += 1;
    }
    else
    {
        if (SymTable_contains(oSymTable , pcKey) != 0)
            return 0;
        else
        {
            Bind *currentBinding = oSymTable[idx].head;
            while(currentBinding->next !=NULL)
            {
                currentBinding = currentBinding->next;
            }
            currentBinding->next = newBinding;
            oSymTable[idx].length += 1;
        }
    }
    return 1;   
}

void* SymTable_get(SymTable_t oSymTable, const char *pcKey )
{
    // assert(oSymTable!=NULL);
    Bind *currentBinding;
    if (pcKey == NULL || strlen(pcKey)==0 || oSymTable == NULL)
        return NULL;
    int idx = SymTable_hash(pcKey,bucketCounts[bucketCountid]);
    currentBinding = oSymTable[idx].head;
    while (currentBinding != NULL)
    {
        if(strcmp (currentBinding->pcKey , pcKey) == 0)
        {
            return currentBinding->pcValue;
        }
        currentBinding = currentBinding->next;
    }
    return NULL;
}


void* SymTable_replace( SymTable_t oSymTable, const char *pcKey, const void *pcValue )
{
    if(oSymTable == NULL)
        return NULL;
    int idx = SymTable_hash(pcKey,bucketCounts[bucketCountid]);
    Bind *temp = oSymTable[idx].head;
    void* pcVa;
    while (temp!= NULL)
    {
        if(strcmp (temp->pcKey , pcKey) == 0)
        {
            pcVa = temp->pcValue;
            temp->pcValue = (void *)pcValue;
            return pcVa;
        }
        temp = temp->next;
    }
    return NULL;
}


void* SymTable_remove (SymTable_t oSymTable,const char *pcKey)
{
    if(oSymTable == NULL || pcKey == NULL || strcmp(pcKey,"")==0)
        return NULL;
    int idx = SymTable_hash(pcKey,bucketCounts[bucketCountid]);
    if(oSymTable[idx].head == NULL)
        return NULL;
    Bind *previous = oSymTable[idx].head;
    Bind *current = oSymTable[idx].head;
    void* pcVal;
    if(current->next == NULL)
    {
        if(strcmp (current->pcKey , pcKey) == 0)
        {
            oSymTable[idx].head=NULL;
            pcVal = current->pcValue;
            Bind_free(current);
            oSymTable[idx].length--;
            return pcVal;
        }
    }
    while (current->next != NULL)
    {
        if(strcmp (current->pcKey , pcKey) == 0)
        {
            if ( current == oSymTable[idx].head)
            {
                oSymTable[idx].head = current->next;
                pcVal = current->pcValue;
                Bind_free(current);
                oSymTable[idx].length--;
                return pcVal;
            }
            previous->next = current->next;
            pcVal = current->pcValue;
            Bind_free(current);
            oSymTable[idx].length--;
            return pcVal;
        }
        previous = current;
        current = current->next;
    }
    if(strcmp (current->pcKey , pcKey) == 0)
    {
        previous->next = current->next;
        pcVal = current->pcValue;
        Bind_free(current);
        oSymTable[idx].length--;
        return pcVal;
    }
    
    return NULL;
}


int SymTable_hash(const char *pcKey, int iBucketCount)
{
    enum {HASH_MULTIPLIER = 65599};
    int i;
    unsigned int uiHash = 0U;
    assert(pcKey != NULL);
    for (i = 0; pcKey[i] != '\0'; i++)
        uiHash = uiHash * (unsigned int) HASH_MULTIPLIER + (unsigned int) pcKey[i];
    return (int)(uiHash % (unsigned int) iBucketCount);
}

SymTable_t SymTable_rehash(SymTable_t oSymTable)
{
    int id,currentid=0;
    SymTable_t *newTable,*curr,*new,*Newcurr;
    Bind *current,*Newcurrent;

    curr = oSymTable;
    if(oSymTable == NULL)
        return 0;
    bucketCountid++;
    newTable = SymTable_new();
    printf("###########\n");
    printf("%d\n",newTable);
    printf("%d\n",bucketCounts[bucketCountid-1]);
    for(int i=0;i<bucketCounts[bucketCountid-1];i++)
    {
        if(curr[i]->length == 0)
            continue;
        printf("curr[i]->length %d\n",curr[i]->head);
        while(curr[i]->head != NULL)
        {
            printf("###########\n");
            SymTable_put(newTable,curr[i]->head->pcKey,curr[i]->head->pcValue);
            curr[i]->head = curr[i]->head->next;
        }
    }
    bucketCountid--;
    // oSymTable = newTable;
    printf("%d\n",oSymTable);
    SymTable_free(oSymTable);
    bucketCountid++;
    return newTable;
}
