#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symtable_hash.c"


/* Helper function to print the binding - key, value pair */
void print_bindings(const char *pcKey, const void *pvValue, void *pvExtra)
{
    if(!strcmp((char *)pvExtra, "int") || !strcmp((char *)pvExtra, "integer"))
        printf("%s %d\n", pcKey, (int *)pvValue);
    else
        printf("%s %s\n", pcKey, (char *)pvValue);
}

/* Unit test for SymTable_new() function */
void test_SymTable_new()
{
    SymTable_t oSymTable1 = SymTable_new();
    assert(oSymTable1 != NULL);
    SymTable_t oSymTable2 = SymTable_new();
    assert(oSymTable2 != NULL);
    SymTable_free(oSymTable1);
    SymTable_free(oSymTable2);
    printf("PASS: test_SymTable_new\n");
}

/* Unit test for SymTable_free() function
   Memory leak can be tested on Valgrind tool. */
void test_SymTable_free()
{
    SymTable_t oSymTable1 = SymTable_new();
    SymTable_free(oSymTable1);
    SymTable_t oSymTable2 = SymTable_new();
    SymTable_free(oSymTable2);
    printf("PASS: test_SymTable_free\n");
}

/* Unit test for SymTable_getLength() function */
void test_SymTable_getLength()
{
    SymTable_t oSymTable = SymTable_new();
    assert(SymTable_getLength(oSymTable) == 0);
    SymTable_put(oSymTable, "key1", "value1");
    printf("---------1------------\n");
    assert(SymTable_getLength(oSymTable) == 1);
    SymTable_put(oSymTable, "key2", "value2");
    printf("---------2------------\n");
    assert(SymTable_getLength(oSymTable) == 2);
    SymTable_put(oSymTable, "key3", "value3");
    printf("---------3------------\n");
    assert(SymTable_getLength(oSymTable) == 3);
    SymTable_put(oSymTable, "key4", "value4");
    printf("---------4------------\n");
    assert(SymTable_getLength(oSymTable) == 4);
    SymTable_put(oSymTable, "key5", "value5");
    printf("---------5------------\n");
    assert(SymTable_getLength(oSymTable) == 5);
    SymTable_put(oSymTable, "key6", "value6");
    printf("---------6------------\n");
    assert(SymTable_getLength(oSymTable) == 6);
    SymTable_put(oSymTable, "key3", "value4");
    SymTable_replace(oSymTable, "key3", "value3_1");
    assert(SymTable_getLength(oSymTable) == 6);
    assert(SymTable_getLength(oSymTable) == 6);
    SymTable_remove(oSymTable, "key3");
    assert(SymTable_getLength(oSymTable) == 5);
    SymTable_remove(oSymTable, "key1");
    assert(SymTable_getLength(oSymTable) == 4);
    SymTable_remove(oSymTable, "key4");
    assert(SymTable_getLength(oSymTable) == 3);
    SymTable_remove(oSymTable, "key1");
    assert(SymTable_getLength(oSymTable) == 3);
    SymTable_free(oSymTable);
    oSymTable = NULL;
    assert(SymTable_getLength(oSymTable) == -1);
    printf("PASS: test_SymTable_getLength\n");
}

/* Unit test for SymTable_put() function */
void test_SymTable_put()
{
    SymTable_t oSymTable = SymTable_new();
    assert(SymTable_put(oSymTable, "key1", "value") == 1);
    assert(SymTable_put(oSymTable, "key2", "random_value") == 1);
    assert(SymTable_put(oSymTable, "key3", "random_value") == 1);
    assert(SymTable_put(oSymTable, "key4", "") == 1);
    assert(SymTable_put(oSymTable, "key1", "new_value") == 0);
    assert(SymTable_put(oSymTable, "key1", "value") == 0);
    assert(SymTable_put(oSymTable, "", "random_value") == 0);
    assert(SymTable_put(oSymTable, NULL, "random_value") == 0);
    SymTable_remove(oSymTable, "key1");
    SymTable_remove(oSymTable, "key4");
    assert(SymTable_put(oSymTable, "key1", "new_value") == 1);
    SymTable_free(oSymTable);
    bucketCountid = 0;
    oSymTable = NULL;
    assert(SymTable_put(oSymTable, "key6", "some_value") == 0);
    SymTable_t oSymTableNew = NULL;
    assert(SymTable_put(oSymTableNew, "key7", "value") == 0);
    printf("PASS: test_SymTable_put\n");
}

/* Unit test for SymTable_contains() function */
void test_SymTable_contains()
{
    SymTable_t oSymTable = SymTable_new();
    SymTable_put(oSymTable, "key1", "value");
    assert(SymTable_contains(oSymTable, "key1") == 1);
    assert(SymTable_contains(oSymTable, "key2") == 0);
    SymTable_put(oSymTable, "", "random_value");
    SymTable_put(oSymTable, NULL, "random_value");
    assert(SymTable_contains(oSymTable, "") == 0);
    assert(SymTable_contains(oSymTable, NULL) == 0);
    SymTable_replace(oSymTable, "key1", "new_value");
    assert(SymTable_contains(oSymTable, "key1") == 1);
    SymTable_remove(oSymTable, "key1");
    assert(SymTable_contains(oSymTable, "key1") == 0);
    SymTable_free(oSymTable);
    SymTable_t oSymTableNew = SymTable_new();
    SymTable_put(oSymTableNew, "key1", "value");
    SymTable_free(oSymTableNew);
    oSymTableNew = NULL;
    assert(SymTable_contains(oSymTableNew, "key1") == 0);
    printf("PASS: test_SymTable_contains\n");
}

/* Unit test for SymTable_get() function */
void test_SymTable_get()
{
    SymTable_t oSymTable = SymTable_new();
    SymTable_put(oSymTable, "key1", "value1");
    SymTable_put(oSymTable, "key2", "value2");
    SymTable_put(oSymTable, "key3", "value3");
    SymTable_put(oSymTable, "key4", "value4");
    SymTable_put(oSymTable, "", "random_value");
    assert(!strcmp(SymTable_get(oSymTable, "key4"), "value4"));
    SymTable_replace(oSymTable, "key4", "new_value4");
    assert(!strcmp(SymTable_get(oSymTable, "key4"), "new_value4"));
    SymTable_remove(oSymTable, "key4");
    assert(SymTable_get(oSymTable, "key4") == NULL);
    assert(SymTable_get(oSymTable, "key5") == NULL);
    assert(SymTable_get(oSymTable, NULL) == NULL);
    assert(SymTable_get(oSymTable, "") == NULL);
    SymTable_free(oSymTable);
    SymTable_t oSymTableNew = SymTable_new();
    SymTable_put(oSymTableNew, "key1", "value");
    SymTable_free(oSymTableNew);
    oSymTableNew = NULL;
    assert(SymTable_get(oSymTableNew, "key1") == NULL);
    printf("PASS: test_SymTable_get\n");
}

/* Unit test for SymTable_replace() function */
void test_SymTable_replace()
{
    SymTable_t oSymTable = SymTable_new();
    SymTable_put(oSymTable, "key1", "value1");
    SymTable_put(oSymTable, "key2", "value2");
    SymTable_put(oSymTable, "key3", "value3");
    SymTable_put(oSymTable, "key4", "value4");
    SymTable_put(oSymTable, "", "random_value");
    SymTable_put(oSymTable, NULL, "random_value");
    assert(!strcmp(SymTable_replace(oSymTable, "key1", "value1_1"), "value1"));
    assert(!strcmp(SymTable_replace(oSymTable, "key2", "value2_1"), "value2"));
    assert(!strcmp(SymTable_replace(oSymTable, "key3", "value3_1"), "value3"));
    assert(!strcmp(SymTable_replace(oSymTable, "key4", "value4_1"), "value4"));
    assert(!strcmp(SymTable_replace(oSymTable, "key4", "value4_2"), "value4_1"));
    assert(SymTable_replace(oSymTable, "key5", "new_value5") == NULL);
    SymTable_free(oSymTable);
    oSymTable = NULL;
    SymTable_t oSymTableNew = SymTable_new();
    SymTable_put(oSymTableNew, "key1", "value");
    SymTable_free(oSymTableNew);
    oSymTableNew = NULL;
    assert(SymTable_replace(oSymTableNew, "key1", "new_value1") == NULL);
    printf("PASS: test_SymTable_replace\n");
}

/* Unit test for SymTable_remove() function */
void test_SymTable_remove()
{
    SymTable_t oSymTable = SymTable_new();
    SymTable_put(oSymTable, "key1", "value1");
    SymTable_put(oSymTable, "key2", "value2");
    SymTable_put(oSymTable, "key3", "value3");
    SymTable_put(oSymTable, "key4", "value4");
    SymTable_put(oSymTable, "", "random_value");
    SymTable_put(oSymTable, NULL, "random_value");
    assert(!strcmp(SymTable_remove(oSymTable, "key4"), "value4"));
    assert(!strcmp(SymTable_remove(oSymTable, "key1"), "value1"));
    assert(SymTable_remove(oSymTable, "key4") == NULL);
    assert(SymTable_remove(oSymTable, "") == NULL);
    assert(SymTable_remove(oSymTable, NULL) == NULL);
    SymTable_free(oSymTable);
    SymTable_t oSymTableNew = SymTable_new();
    SymTable_put(oSymTableNew, "key1", "value");
    SymTable_free(oSymTableNew);
    oSymTableNew = NULL;
    assert(SymTable_remove(oSymTableNew, "key1") == NULL);
    printf("PASS: test_SymTable_remove\n");
}

/* Unit test for SymTable_map() function */
void test_SymTable_map()
{
    void (*function_ptr) (const char *pcKey, const void *pvValue, void *pvExtra) = print_bindings;
    SymTable_t oSymTable1 = SymTable_new();
    SymTable_put(oSymTable1, "key1", "value1");
    SymTable_put(oSymTable1, "key2", "value2");
    SymTable_put(oSymTable1, "key3", "value3");
    SymTable_put(oSymTable1, "key4", "value4");
    printf("SymTable: oSymTable1\n");
    SymTable_map(oSymTable1, (*function_ptr), "");
    SymTable_free(oSymTable1);

    oSymTable1 = NULL;
    SymTable_map(oSymTable1, (*function_ptr), "");
    SymTable_t oSymTable2 = SymTable_new();
    SymTable_put(oSymTable2, "key1", 1);
    SymTable_put(oSymTable2, "key2", -2);
    SymTable_put(oSymTable2, "key3", 3);
    SymTable_put(oSymTable2, "key3", -4);
    printf("SymTable: oSymTable2\n");
    SymTable_map(oSymTable2, (*function_ptr), "integer");
    SymTable_free(oSymTable2);
    
    oSymTable2 = NULL;
    SymTable_map(oSymTable2, (*function_ptr), "integer");
    
    oSymTable2 = NULL;
    printf("PASS: test_SymTable_map\n");
}

int main()
{
    test_SymTable_new();
    test_SymTable_free();
    test_SymTable_getLength();
    test_SymTable_put();
    test_SymTable_get();
    test_SymTable_contains();
    test_SymTable_replace();
    test_SymTable_remove();
    test_SymTable_map();
    printf("ALL TEST CASES PASSED\n");
    return 0;
}