#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include<assert.h>

typedef struct BIND Bind;
typedef struct SymTable *SymTable_t;

SymTable_t SymTable_new(void); // Creation of new entry in SymTable

void SymTable_free( SymTable_t oSymTable ); // freeing the particular entry of SymTable

int SymTable_getLength( SymTable_t oSymTable ); // Getting the Length of the SymTable

void SymTable_map( SymTable_t oSymTable, void ( *pfApply ) (const char *pcKey, const void *pvValue, void *pvExtra),const void *pvExtra ); // Changes all the bindings in the SymTable

int SymTable_contains( SymTable_t oSymTable, const char *pcKey ); // Searching of the particular value

int SymTable_put( SymTable_t oSymTable, const char *pcKey, const void *pcValue ); //insertion of the binding in SymTable

void* SymTable_get( SymTable_t oSymTable, const char *pcKey ); //getting the particular binding value

void* SymTable_replace( SymTable_t oSymTable, const char *pcKey, const void *pcValue ); // Replaces the particular value with new one

void* SymTable_remove( SymTable_t oSymTable, const char *pcKey);